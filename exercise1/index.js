require('process');
const AWS = require('aws-sdk');

exports.handler = async (event, context) => {
    let name = "Larry";
    const key = process.env.API_KEY;
    const docClient = new AWS.DynamoDB.DocumentClient();
    if (key == undefined) {
        throw new Error("Missing required configuration API_KEY");
    }
    
    // Validate auth
    if (event.headers["x-api-key"] !== key ) {
        return {
            statusCode: 401,
            body: JSON.stringify({'error': 'unauthenticated'}),
            headers: {
                "Content-Type": "application/json"
            }
        };
    }
    if (event.queryStringParameters !== undefined) {
        name = event.queryStringParameters.name ?? "Larry";
    }
    
    // Put the name in dynamo
    const params = {
        TableName : 'wealth-wizards',
        Item: {
            id: Math.random().toString(16).substr(2, 10),
            name: name,
            createdAt: new Date().getTime()
        }
    };
    
    await docClient.put(params).promise();
    
    const response = {
        statusCode: 200,
        body: JSON.stringify({'msg': 'Hello from ' + name.charAt(0).toUpperCase() + name.slice(1) + '!'}),
        headers: {
            "Content-Type": "application/json"
        }
    };
    return response;
};

