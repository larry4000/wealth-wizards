## Exercise 1

My Lambda is available on

https://raaokyz7fb.execute-api.eu-west-2.amazonaws.com

It's using the $default route, so all paths will fall through to the same code. You can customise the name outputted in the response with the ?name parameter. All names are saved in a DynamoDB table.

![](images/dynamodb.png)

Using the endpoint requires authentication. I have implemented a simple "X-Api-Key" header within the lambda code.
