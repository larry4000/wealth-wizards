terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "larry-hack"
  region  = "eu-west-2"
  # Default tags on all resources (Step 7)
  default_tags {
    tags = {
      Name       = "Wealth Wizards Test"
      Environment = "dev"
    }
  }
}

# Get the current user id for the bucket policy
data "aws_caller_identity" "current" {}

# Make a bucket (Step 3)
resource "aws_s3_bucket" "ww-bucket" {
  bucket = "wealth-wizards"
  acl    = "private"

  # Enable versioning (Step 5)
  versioning {
    enabled = true
  }

  # Enable server side encryption (Step 5)
  server_side_encryption_configuration {
      rule {
          apply_server_side_encryption_by_default {
            sse_algorithm = "AES256"  
          }
      }
  }
}

# Block all public access to this bucket (Step 4)
resource "aws_s3_bucket_public_access_block" "ww-block" {
  bucket = aws_s3_bucket.ww-bucket.id

  # Block all the things
  block_public_acls   = true
  block_public_policy = true
  ignore_public_acls = true
  restrict_public_buckets = true
}

# Create a role that can be assumed by Lambda
# and can upload to the bucket (Step 8)
resource "aws_iam_role" "ww-role" {
  name = "ww-s3-role"

  # Allow role to be assumed by Lambda
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })

  # Inlining a policy to allow PutObject on our bucket specifically
  inline_policy {
    name = "ww-s3-upload-policy"
    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action = [
            "s3:PutObject",
          ]
          Effect   = "Allow"
          Resource = "${aws_s3_bucket.ww-bucket.arn}/*"
        },
      ]
    })
  }
}



# Add bucket policy (Step 7). Think that (?) you're alluding to
# using a bucket policy here. I looked at the following AWS blog post:
# https://aws.amazon.com/blogs/security/how-to-restrict-amazon-s3-bucket-access-to-a-specific-iam-role/
# It sounds like they don't recommend this for single account usage, but I've put it in anyway
resource "aws_s3_bucket_policy" "ww-policy" {
  bucket = aws_s3_bucket.ww-bucket.id

  policy = jsonencode({
    "Version": "2012-10-17",
        "Statement": [
          {
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:*",
            "Resource": [
              aws_s3_bucket.ww-bucket.arn,
              "${aws_s3_bucket.ww-bucket.arn}/*",
            ],
            "Condition": {
              "StringNotLike": {
                  "aws:userId": [
                    "${aws_iam_role.ww-role.unique_id}:*",
                    # My user account for CLI
                    data.aws_caller_identity.current.user_id,
                    # Root account just in case it all breaks
                    data.aws_caller_identity.current.account_id,
                  ]
              }
            }
          }
        ]
    })
}

