## Exercise 2

### What is Terraform

Terraform is a infrastructure as code solution made by HashiCorp. Resources to be provisioned are expressed using HCL configuration files. These files typically declare things like VMs, networks, databases, permission sets, etc. Terraform provides a commandline tool to take these HCL declarations and map them into specific cloud provider instructions. This tool also provides state management – Terraform will diff the HCL files (desired state) against the current state from the cloud provider, and decide on the optimal changes in order to reach desired state.

### Why would we use it?
Automating creation of infra with a tool like Terraform has numerous advantages:
- Provisioning of resources is more reliable than humans clicking on buttons in e.g. AWS console
- Provisioning and removal of resources can be done much more quickly at scale than relying on manual processes
- Brings developers closer to infrastructure, by expressing it in a way we understand (code)
- The provisioning of infrastructure can be run as part of CI/CD workflows e.g. Gitlab Pipelines
- By handling desired/current state, changing complex infrastructure is made much simpler
- Terraform supports different Cloud Providers, so can work multi-cloud if desired

### Terraform Coding
See main.tf for the rest of the exercise. Step 9 evidence:

![](images/s3-cli.png)
